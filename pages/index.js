import { sanityClient, urlFor } from '../sanity'
import Link from 'next/link'
import { isMultiple } from '../utils'
import DashboardMap from '../components/DashboardMap'

// pass through properties from calling the sanity serverside
const Home = ({ properties }) => {
  console.log(properties)
  return (
    <>
      {properties.length > 0 && (
        <div className="main">
          <div className="feed-container">
            <h1>Places to stay near you</h1>
            <div className="feed">
              {properties.map((property, index) => (
                <Link href={`property/${property.slug.current}`} >
                  <div className="card" key={index}>
                    <img src={urlFor(property.mainImage)} alt={property.title} />
                    <h3>{property.title}</h3>
                    <p>{property.reviews.length} review{isMultiple(property.reviews.length)}</p>
                    <p>${ property.pricePerNight } per night</p>
                  </div>
                </Link>
              ))}
            </div>
            <div className="map">
              <DashboardMap properties={properties} />
            </div>
          </div>
        </div>
      )}
    </>
  )
}

// come back to this for nextJS
// must be named getServerSideProps
export const getServerSideProps = async () => {
  const query = '*[ _type == "property"]'
  const properties = await sanityClient.fetch(query)
  if (!properties.length) {
    return {
      props: {
        properties: [],
      }
    }
  } else {
    return {
      props: {
        properties
      }
    }
  }
}

export default Home