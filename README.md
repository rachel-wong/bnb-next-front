## Front side to the BNB-next app

> The purpose of this app is to experiment with nextJS and sanity IO content modelling. **UI was not the focus, rather on linking correct content modelling to react.**

To run `npm run dev`

## Learning notes
* need a sanity client in the root to connect to the sanity backend
* need next-sanity package to be installed for the sanity client
* remove the boilerplate in /pages/index.js to start
* the project ID and the dataset key in the sanity client needs to be stored in the .env of the front end root. The secret keys can be found in the backend sanity.json
* remember that any time .env is changed to restart the npm run dev server again
* sanity.js is copied/pasted config
* need to use `urlFor` from sanity client in order to display images correctly
* there was an issue with mapping out the nested array of images from property. Instead of mapping out the array with the image attribute this was the solution:

```javascript
  {images.map(({ _key, asset }, image) => (
    <Image key={_key} identifier="image" image={asset} />
  ))}
```

* when displaying nested attributes, always nullcheck as you move each level down to prevent app breaking should some of the fields are not fully filled in. For example: `property?.location?.lat`
* refer to `[slug].js` for how a single property is mapped out.
* helper methods is best grouped under utils. But remember to destructure when importing. `import {isMultiple } from '../utils`
