import React from 'react'
import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';

const DashboardMap = ({ properties }) => {

  const containerStyle = {
    width: '100%',
    height: '100%'
  };

  console.log(properties[0].location?.lat)
  // make the map center on the first location instead of overly zoomed out
  const center = {
    lat: properties[0].location?.lat,
    lng: properties[0].location?.lng,
  }

  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: process.env.NEXT_GOOGLE_MAP_API_KEY,
  })

  const [map, setMap] = React.useState(null)

  const onLoad = React.useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds();
    map.fitBounds(bounds);
    setMap(map)
  }, [])

  const onUnmount = React.useCallback(function callback(map) {
    setMap(null)
  }, [])

  const image = "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png"

  return isLoaded ? (
      <GoogleMap
      mapContainerStyle={containerStyle}
      center={center}
      zoom={5}
      onLoad={onLoad}
      onUnmount={onUnmount}
    >
      {properties.length > 0 && properties.map((property, idx) => (
        <Marker key={property?._key}
        position={{ lat: property?.location?.lat, lng: property?.location?.lng }}
        icon={{
          url: image,
          anchor: new google.maps.Point(5, 58),
        }}
      />
      ))}
      <></>
    </GoogleMap>
  ) : <></>
}

export default DashboardMap